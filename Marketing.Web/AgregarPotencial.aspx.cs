﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Marketing.Web
{
    public partial class AgregarPotencial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
                return;
            var currentid = Request.QueryString["id"];
            //Si hay un Id en el Get lleno la información del Cliente
            if (string.IsNullOrEmpty(currentid)) return;
            try
            {
                var client = Lib.Partial.Cliente.GetClientebyPk(currentid);
                if (string.IsNullOrEmpty(client.Id))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No se ha encontrado la información del cliente.');", true);
                    return;
                }
                txtId.Enabled = false;
                txtId.Text = client.Id;
                txtNombre.Text = client.Nombre;
                txtApellido.Text = client.Apellido;
                txtCorreo.Text = client.Correo;
                txtFechaNac.Date = client.FechaNac;
                lblEdad.Text = DateTime.Now.AddYears(-txtFechaNac.Date.Year).Year.ToString();
            }
            catch (Exception ex)
            {
                //Log de Errores (No debe se visualizado por el cliente)
            }

        }

        protected void txtFechaNac_DateChanged(object sender, EventArgs e)
        {
            try
            {
                lblEdad.Text = DateTime.Now.AddYears(-txtFechaNac.Date.Year).Year.ToString();

            }
            catch (Exception ex)
            {
                //Log de Errores (No debe se visualizado por el cliente)
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            //Verifico Año de la fecha de nac
            if (txtFechaNac.Date.Year >= DateTime.Now.Year)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Por favor coloque una fecha anterior a este año.');", true);
                return;
            }
            var current = Request.QueryString["id"];
            //Si hay un Id en el Get modifica sino Agrega
            if (string.IsNullOrEmpty(current))
            {
                var clienteExite = Lib.Partial.Cliente.GetClientebyPk(txtId.Text);
                //Certifico que el Id no este registrado
                if (clienteExite!=null)
                {
                    //No Hay Id  en el Get
                    var newCliente = new Lib.Partial.Cliente
                    {
                        Id = txtId.Text,
                        Nombre = txtNombre.Text,
                        Apellido = txtApellido.Text,
                        Correo = txtCorreo.Text,
                        FechaNac = txtFechaNac.Date
                    };
                    newCliente.Save();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert",
                        "alert('Se guardo el cliente correctamente.');", true);
                    return;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert",
                      "alert('No se pudo guardar el registro ya el Id esta en uso.');", true);
                    return;
                }

            }
            //Hay un Id en el Get para modificar datos
            var cliente = Lib.Partial.Cliente.GetClientebyPk(current);
            //Si ya existe el Id da el mensaje al usuario
            cliente.Nombre = txtNombre.Text;
            cliente.Apellido = txtApellido.Text;
            cliente.Correo = txtCorreo.Text;
            cliente.FechaNac = txtFechaNac.Date;
            cliente.Save();
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Se actualizo el cliente correctamente.');", true);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                //Busca las filas seleccionadas
                var current = Request.QueryString["id"];
                //Si no hay ninguna envia una alerta al usuario
                if (string.IsNullOrEmpty(current))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Por favor seleccione un cliente.');", true);
                    return;
                }
                //Busca el cliente y lo elimina
                var cliente = Lib.Partial.Cliente.GetClientebyPk(current);
                cliente.Delete();
            }
            catch (Exception ex)
            {
                //Cualquier error debe enviarlo al Log de errores

            }
        }

        protected void txtCorreo_TextChanged(object sender, EventArgs e)
        {
            RegularExpressionValidator1.Enabled = txtCorreo.Text.Length > 0;
        }
    }
}