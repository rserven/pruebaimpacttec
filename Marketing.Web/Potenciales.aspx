﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Potenciales.aspx.cs" Inherits="Marketing.Web.Potenciales" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 50%;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        var lastId = 0;
        //Se actualiza la Info del Grid cada vez que se realiza un clic
        function grid_SelectionChanged(s, e) {
            s.GetSelectedFieldValues("Id", GetSelectedFieldValuesCallback);
        }

        //Se guarda el Id de la fila seleccionada
        function GetSelectedFieldValuesCallback(values) {lastId = values;}

        //Para visualizar el detalle del cliente
        function OpenWindow() {
            if (lastId == 0) {
                alert('Seleccione un cliente para continuar.');
                return false;
            }
            window.open('AgregarPotencial.aspx?id=' + lastId, null, 'height=400,width=400,status=yes,toolbar=no,menubar=no,location=no');
            return true;
        }

        //Abre unpopu para un nuevo registro
        function OpenWindowNew() {
           
            window.open('AgregarPotencial.aspx', null, 'height=400,width=400,status=yes,toolbar=no,menubar=no,location=no');
            return true;
        }
    </script>
    <form id="form1" runat="server">
        <div>

        </div>

            <table class="auto-style1"><tr>
                    <td>
                        <table class="dx-justification">
                            <tr>
                                <td>
                                    <dx:ASPxTextBox ID="txtId" runat="server" Width="170px" NullText="Id">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txtNombre" runat="server" Width="170px" NullText="Nombre">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxTextBox ID="txtApellido" runat="server" Width="170px" NullText="Apellido">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txtCorreo" runat="server" Width="170px" NullText="Correo">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxSpinEdit ID="txtEdad" runat="server" NullText="Edad" Number="0" />
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnBuscarMayores" runat="server" OnClick="btnBuscarMayores_Click" Text="Buscar Potenciales">
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnBuscar" runat="server" OnClick="btnBuscar_Click" Text="Buscar">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </tr>
                <tr>
                    <td>
                        <dx:ASPxGridView ID="GridPotenciales" runat="server" KeyFieldName="Id" Width="100%">

                            <SettingsBehavior AllowSelectByRowClick="true" />
                            <Columns>
                                <dx:GridViewDataColumn FieldName="Id" VisibleIndex="1" />
                                <dx:GridViewDataColumn FieldName="Nombre" VisibleIndex="2" />
                                <dx:GridViewDataColumn FieldName="Apellido" VisibleIndex="3" />
                                <dx:GridViewDataColumn FieldName="Correo" VisibleIndex="4" />
                                <dx:GridViewDataColumn FieldName="Edad" VisibleIndex="5" />
                            </Columns>
                            <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
                        </dx:ASPxGridView>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="dxflInternalEditorTable">
                            <tr>
                                <td><dx:ASPxButton ID="btnInsertar" runat="server" Text="Insertar">
                                        <ClientSideEvents Click="function (s, e) {OpenWindowNew()}" />
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnVer" runat="server" Text="Ver Detalle">
                                        <ClientSideEvents Click="function (s, e) {OpenWindow()}" />
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnEliminar" runat="server" Text="Borrar" OnClick="btnEliminar_Click">
                                        <ClientSideEvents Click="function(s,e) { e.processOnServer = confirm('¿Desea eliminar el registro?'); }" />
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

    </form>
</body>
</html>
