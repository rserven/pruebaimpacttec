﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Marketing.Web
{
    public partial class Potenciales : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridPotenciales.DataSource = Lib.Partial.Cliente.GetAllClientes();
            GridPotenciales.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            var edad = 0;
            //Si la Edad es diferente de 0 se calcula el año de nacimiento del comodin para realizar la busqueda
            if (txtEdad.Text != "0")
            {
                edad = DateTime.Now.AddYears(-Convert.ToInt32(txtEdad.Text)).Year;
            }
            GridPotenciales.DataSource = Lib.Partial.Cliente.GetAllClientes(x => (txtId.Text != string.Empty && x.Id.Contains(txtId.Text)) || (txtNombre.Text != string.Empty && x.Nombre.Contains(txtNombre.Text)) || (txtApellido.Text != string.Empty && x.Apellido.Contains(txtApellido.Text)) || (txtCorreo.Text != string.Empty && x.Correo.Contains(txtCorreo.Text)) || (edad >= 0 && x.FechaNac.Year == edad));
            GridPotenciales.DataBind();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Busca las filas seleccionadas
                var current = GridPotenciales.GetSelectedFieldValues("Id");
                //Si no hay ninguna envia una alerta al usuario
                if (!current.Any())
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Por favor seleccione un cliente de la tabla.');", true);
                    return;
                }
                //Si hay algun registro, lo selecciona
                var currentId = current[0];
                //Busca el cliente y lo elimina
                var cliente = Lib.Partial.Cliente.GetClientebyPk(currentId.ToString());
                cliente.Delete();

                //Actualiza el Grid
                GridPotenciales.DataSource = Lib.Partial.Cliente.GetAllClientes();
                GridPotenciales.DataBind();
            }
            catch (Exception ex)
            {
                //Cualquier error debe enviarlo al Log de errores

            }


        }

        protected void btnBuscarMayores_Click(object sender, EventArgs e)
        {
            var edad = DateTime.Now.AddYears(-18).Year;
            GridPotenciales.DataSource = Lib.Partial.Cliente.GetAllClientes(x => x.FechaNac.Year <= edad);
            GridPotenciales.DataBind();
        }


    }
}