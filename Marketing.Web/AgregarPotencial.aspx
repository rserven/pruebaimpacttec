﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgregarPotencial.aspx.cs" Inherits="Marketing.Web.AgregarPotencial" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .dxeTrackBar,
        .dxeIRadioButton,
        .dxeButtonEdit,
        .dxeTextBox,
        .dxeRadioButtonList,
        .dxeCheckBoxList,
        .dxeMemo,
        .dxeListBox,
        .dxeCalendar,
        .dxeColorTable {
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        }

        .dxeTextBox,
        .dxeButtonEdit,
        .dxeIRadioButton,
        .dxeRadioButtonList,
        .dxeCheckBoxList {
            cursor: default;
        }

        .dxeTextBox {
            background-color: white;
            border: 1px solid #9f9f9f;
            font: 12px Tahoma, Geneva, sans-serif;
        }

        .dxeTextBoxDefaultWidthSys,
        .dxeButtonEditSys {
            width: 170px;
        }

        .dxeTextBoxSys,
        .dxeMemoSys {
            border-collapse: separate !important;
        }

        .dxeMemoEditAreaSys, /*Bootstrap correction*/
        input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
        input[type="password"].dxeEditAreaSys /*Bootstrap correction*/ {
            display: block;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            -webkit-transition: none;
            -moz-transition: none;
            -o-transition: none;
            transition: none;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
        }

        .dxeEditAreaSys,
        .dxeMemoEditAreaSys, /*Bootstrap correction*/
        input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
        input[type="password"].dxeEditAreaSys /*Bootstrap correction*/ {
            font: inherit;
            line-height: normal;
            outline: 0;
        }

        input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
        input[type="password"].dxeEditAreaSys /*Bootstrap correction*/ {
            margin-top: 0;
            margin-bottom: 0;
        }

        .dxeEditAreaSys,
        input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
        input[type="password"].dxeEditAreaSys /*Bootstrap correction*/ {
            padding: 0px 1px 0px 0px; /* B146658 */
        }

        .dxeTextBox .dxeEditArea {
            background-color: white;
        }

        .dxeEditArea {
            border: 1px solid #A0A0A0;
        }

        .dxeEditAreaSys {
            border: 0px !important;
            background-position: 0 0; /* iOS Safari */
            -webkit-box-sizing: content-box; /*Bootstrap correction*/
            -moz-box-sizing: content-box; /*Bootstrap correction*/
            box-sizing: content-box; /*Bootstrap correction*/
        }

        .width:100% {
            width: 683px;
        }

        .auto-style2 {
            width: 158px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table style="width: 500px">
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                </tr>
                        <tr>
                            <td>Identificador</td>
                                <td>
                                    <dx:ASPxTextBox ID="txtId" runat="server" Width="170px" NullText="Id">
                                    </dx:ASPxTextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtId" ErrorMessage="Identificador es Requerido" style="color: #FF0000">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Nombres</td>
                                <td>
                                    <dx:ASPxTextBox ID="txtNombre" runat="server" Width="170px" NullText="Nombre">
                                    </dx:ASPxTextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtNombre" ErrorMessage="Identificador es Requerido" style="color: #FF0000">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Apellidos</td>
                                <td>
                                    <dx:ASPxTextBox ID="txtApellido" runat="server" Width="170px" NullText="Apellido">
                                    </dx:ASPxTextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtApellido" ErrorMessage="Identificador es Requerido" style="color: #FF0000">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Edad</td>
                                <td>
                                    <table class="auto-style1">
                                        <tr>
                                            <td class="auto-style2">
                                                <dx:ASPxDateEdit ID="txtFechaNac" runat="server" AutoPostBack="True" OnDateChanged="txtFechaNac_DateChanged">
                                                </dx:ASPxDateEdit>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEdad" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>Correo</td>
                                <td>
                                    <dx:ASPxTextBox ID="txtCorreo" runat="server" Width="170px" NullText="Correo" AutoPostBack="True" OnTextChanged="txtCorreo_TextChanged">
                                    </dx:ASPxTextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCorreo" Enabled="False" ErrorMessage="Correo invalido" style="color: #FF0000" ToolTip="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click">
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnSalir" runat="server" Text="Salir">
                                        <ClientSideEvents Click="function (s, e) {window.close()}" />
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnBorrar" runat="server" Text="Borrar" OnClick="btnBorrar_Click">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
