﻿//------------------------------------------------------------------------------
//  Renier Serven. 26-04-2016 10:44:22 p.m.
//  
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Data;
using System.Reflection;
using Marketing.Lib.Model;

namespace Marketing.Lib.Partial
{
    public partial class Cliente
    {
        #region Member Variables
        #endregion

        #region Public Properties
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Id { get; set; }
        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [StringLength(150)]
        public string Nombre { get; set; }
        /// <summary>
        /// Apellido
        /// </summary>
        [Required]
        [StringLength(150)]
        public string Apellido { get; set; }
        /// <summary>
        /// Correo
        /// </summary>
        [StringLength(150)]
        public string Correo { get; set; }
        /// <summary>
        /// FechaNac
        /// </summary>
        [Required]
        public DateTime FechaNac { get; set; }
        public int Edad
        {
            get { return DateTime.Now.AddYears(-FechaNac.Year).Year; }

        }

        #endregion

        #region Constructors
        /// <summary>
        /// Create a New Instance of Cliente
        /// </summary>        
        public Cliente()
        { }

        /// <summary>
        /// Create a new instance from the model
        /// </summary>
        /// <param name="cliente">Object</param>
        private Cliente(Mantenimiento_Cliente cliente)
        {
            Id = cliente.Id;
            Nombre = cliente.Nombre;
            Apellido = cliente.Apellido;
            if (cliente.Correo != null)
                Correo = cliente.Correo;
            FechaNac = cliente.FechaNac;
        }
        #endregion

        #region Execute Methods
        /// <summary>
        /// Get All Clientes
        /// </summary>
        /// <returns>IEnumerable Cliente</returns>
        public static IEnumerable<Cliente> GetAllClientes()
        {
            try
            {
                var entity = new MarketingEntities();
                var clientes = entity.Mantenimiento_Cliente.ToList();
                return clientes.Select(eft => new Cliente(eft)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Get All Clientes filtered with Lambda expression
        /// </summary>
        /// <param name="predicate">Lambda expression to filter</param>
        /// <returns>IEnumerable Cliente</returns>
        public static IEnumerable<Cliente> GetAllClientes(Func<Mantenimiento_Cliente, bool> predicate)
        {
            try
            {
                var entity = new MarketingEntities();
                var clientes = entity.Mantenimiento_Cliente.Where(predicate).ToList();
                return clientes.Select(eft => new Cliente(eft)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Get Cliente by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Cliente</returns>
        public static Cliente GetClientebyPk(string id)
        {
            try
            {
                var entity = new MarketingEntities();
                var cliente = entity.Mantenimiento_Cliente.Find(id);
                return cliente != null ? new Cliente(cliente) : null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Save current Cliente
        /// </summary>
        /// <returns>Returns true if saved</returns>
        public bool Save()
        {
            var saved = false;
            using (var transaction = new System.Transactions.TransactionScope())
            {
                try
                {
                    var entity = new MarketingEntities();
                    var cliente = entity.Mantenimiento_Cliente.FirstOrDefault(x => x.Id == Id);
                    if (cliente != null)
                    {
                        cliente.Id = Id;
                        cliente.Nombre = Nombre;
                        cliente.Apellido = Apellido;
                        cliente.Correo = Correo;
                        cliente.FechaNac = FechaNac;

                        entity.Entry(cliente).State = EntityState.Modified;
                        entity.SaveChanges();entity.Dispose(); 
                    }
                    else
                    {
                        var newcliente = new Mantenimiento_Cliente
                        {
                            Id = Id,
                            Nombre = Nombre,
                            Apellido = Apellido,
                            Correo = Correo,
                            FechaNac = FechaNac

                        };
                        entity.Mantenimiento_Cliente.Add(newcliente);
                        entity.SaveChanges();
                        entity.Entry(newcliente).GetDatabaseValues();
                        entity.Dispose();

                        Id = newcliente.Id;
                    }

                    saved = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    if (saved)
                    {
                        transaction.Complete();
                    }
                }
            }
            return true;
        }

        public void Delete()
        {
            var entity = new MarketingEntities();
            var cliente = entity.Mantenimiento_Cliente.FirstOrDefault(x => x.Id == Id);
            if (cliente == null) return;
            entity.Entry(cliente).State = EntityState.Deleted;
            entity.SaveChanges();
            entity.Dispose();
        }

        #endregion
    }
}